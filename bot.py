"""
A simple PTB Bot the receives images and stores them for the use in mcr.py
"""
import os
from telegram.ext import MessageHandler, Filters, Updater, CommandHandler

import logging
# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def store_image(update, context):
    try:
        photo = update.message.photo[-1]
        image = context.bot.get_file(photo.file_id)
        image.download('hint_image.png')
        update.message.reply_text('Okilidokili')
    except:
        update.message.reply_text('Nope, da ging was schief. Versuch\'s noch mal')

def update_hint(update, context):
    hint = update.message.text
    with open('hint', 'w') as file:
        file.write(hint)
    if os.path.exists('hint_image.png'):
        os.remove('hint_image.png')
    update.message.reply_text('Aye, Aye!')

def remove_image(update, context):
    if os.path.exists('hint_image.png'):
        os.remove('hint_image.png')
        update.message.reply_text('Weg damit!')
    else:
        update.message.reply_text('Kein Bild als Hinweis gesetzt')

def start(update, context):
    with open('status', 'w') as file:
        file.write('running')
    update.message.reply_text('Los geht\'s!')

def win(update, context):
    with open('status', 'w') as file:
        file.write('won')
    update.message.reply_text('Glückwunsch :)')

def reset(update, context):
    if os.path.exists('hint_image.png'):
        os.remove('hint_image.png')
    with open('hint', 'w') as file:
        file.write('Verbleibende Zeit bis zur Räumung:')
    with open('status', 'w') as file:
        file.write('idle')
    update.message.reply_text('Okay. Alles bereit für den nächsten Durchgang. Das Spiel beginn auf /start')

def main():
    updater = Updater('1075514971:AAGkFN61wKNF_UvspnnATY92n8mA8DKynm0', use_context=True)
    dp = updater.dispatcher

    # Order matters!
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('win', win))
    dp.add_handler(CommandHandler(['reset', 'end'], reset))
    dp.add_handler(CommandHandler('remove_image', remove_image))
    dp.add_handler(MessageHandler(Filters.photo, store_image))
    dp.add_handler(MessageHandler(Filters.text, update_hint))

    updater.start_polling()

    updater.idle()

if __name__ == '__main__':
    main()
